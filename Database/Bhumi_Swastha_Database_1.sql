-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema bhumi_swachta
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema bhumi_swachta
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bhumi_swachta` DEFAULT CHARACTER SET utf8 ;
USE `bhumi_swachta` ;

-- -----------------------------------------------------
-- Table `bhumi_swachta`.`localites`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bhumi_swachta`.`localites` (
  `name` VARCHAR(50) NOT NULL,
  `email_id` VARCHAR(50) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `localite_id` VARCHAR(50) NOT NULL,
  `state` VARCHAR(45) NULL DEFAULT NULL,
  `city` VARCHAR(45) NULL DEFAULT NULL,
  `no_uploaded` INT(10) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`name`, `localite_id`),
  UNIQUE INDEX `email_id_UNIQUE` (`email_id` ASC),
  UNIQUE INDEX `password_UNIQUE` (`password` ASC),
  UNIQUE INDEX `localite_id_UNIQUE` (`localite_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Information stored about the localites.';


-- -----------------------------------------------------
-- Table `bhumi_swachta`.`surveyor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bhumi_swachta`.`surveyor` (
  `name` VARCHAR(50) NOT NULL,
  `email_id` VARCHAR(50) NOT NULL,
  `password` VARCHAR(50) NOT NULL,
  `surveyer_id` VARCHAR(45) NOT NULL,
  `no_surveyed` INT(10) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`name`, `surveyer_id`),
  UNIQUE INDEX `email_id_UNIQUE` (`email_id` ASC),
  UNIQUE INDEX `password_UNIQUE` (`password` ASC),
  UNIQUE INDEX `surveyer_id_UNIQUE` (`surveyer_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bhumi_swachta`.`photos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bhumi_swachta`.`photos` (
  `photo_id` VARCHAR(100) NOT NULL,
  `uploader` VARCHAR(50) NOT NULL,
  `surveyor` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`photo_id`),
  UNIQUE INDEX `photo_id_UNIQUE` (`photo_id` ASC),
  INDEX `uploader_idx` (`uploader` ASC),
  INDEX `surveyor_idx` (`surveyor` ASC),
  CONSTRAINT `surveyor`
    FOREIGN KEY (`surveyor`)
    REFERENCES `bhumi_swachta`.`surveyor` (`surveyer_id`),
  CONSTRAINT `uploader`
    FOREIGN KEY (`uploader`)
    REFERENCES `bhumi_swachta`.`localites` (`localite_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bhumi_swachta`.`location`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bhumi_swachta`.`location` (
  `location_id` VARCHAR(50) NOT NULL,
  `photo_id` VARCHAR(50) NOT NULL,
  `g_location_id` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`location_id`),
  UNIQUE INDEX `location_id_UNIQUE` (`location_id` ASC),
  UNIQUE INDEX `photo_UNIQUE` (`photo_id` ASC),
  UNIQUE INDEX `g_id_UNIQUE` (`g_location_id` ASC),
  CONSTRAINT `photo_id`
    FOREIGN KEY (`photo_id`)
    REFERENCES `bhumi_swachta`.`photos` (`photo_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bhumi_swachta`.`ministry_personnel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bhumi_swachta`.`ministry_personnel` (
  `name` VARCHAR(50) NOT NULL,
  `email_id` VARCHAR(50) NOT NULL,
  `password` VARCHAR(50) NOT NULL,
  `personnel_id` VARCHAR(50) NOT NULL,
  `no_surveyed` INT(10) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE INDEX `email_id_UNIQUE` (`email_id` ASC),
  UNIQUE INDEX `password_UNIQUE` (`password` ASC),
  UNIQUE INDEX `personnel_id_UNIQUE` (`personnel_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
