from django.shortcuts import redirect
from .forms import EmailForm
from django.http import HttpResponseRedirect,JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json

# Create your views here.
@csrf_exempt
def getEmail(request):

    if False:
        if request.method == 'POST':
            form=EmailForm(request.POST)
            if form.is_valid():
                email = form.cleaned_data['email']
                # pymysql/sqlite code to store data
                return HttpResponseRedirect()#enter url here

        return redirect('/add_url_here')

    if request.method == 'POST':
        rx_dict = json.loads(request.body.decode("utf-8"))
        return JsonResponse(rx_dict)

